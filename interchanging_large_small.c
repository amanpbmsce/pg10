#include <stdio.h>
int main()
{
    int n,i,c,min,max,min_p=0,max_p=0;
    printf("Enter the size of the array\n");
    scanf("%d",&n);
    int a[n];
    printf("Enter the elements of the array\n");
    for(i=0;i<n;i++)
    {
        scanf("%d",&a[i]);
    }
    min=a[0];
    max=a[0];
    for(i=0;i<n;i++)
    {
        if(a[i]<=min)
        {
            min=a[i];
            min_p=i;
        }
        else
        {
            max=a[i];
            max_p=i;
        }
    }
    c=a[max_p];
    a[max_p]=a[min_p];
    a[min_p]=c;
    printf("Array after interchanging the smallest and greatest elements : ");
    for(i=0;i<n;i++)
    {
        printf("%d ",a[i]);
    }
    return 0;
}