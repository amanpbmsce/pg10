#include<stdio.h>
int main()
{
    struct details
    {
        char name[100];
        float cgpa;
        int ID;
        float fees;
        struct DOB
        {
            int dd;
            int mm;
            int yy;
        }b;
    }s;
    printf("Student Details\n");
    printf("\nStudent Name : ");
    scanf("%s",s.name);
    printf("\nStudent ID : ");
    scanf("%d",&s.ID);
    printf("\nDate of Birth : ");
    scanf("%d %d %d",&s.b.dd,&s.b.mm,&s.b.yy);
    printf("\nStudent monthly fees : ");
    scanf("%f",&s.fees);
    printf("\nStudent CGPA : ");
    scanf("%f",&s.cgpa);
    printf("Name : %s",s.name);
    printf("\tID : %d",s.ID);
    printf("\tDate of Birth : %d/%d/%d",s.b.dd,s.b.mm,s.b.yy);
    printf("\tMonthly Fees: %.2f",s.fees);
    printf("\tCGPA : %.1f\n",s.cgpa);
    return 0;
}