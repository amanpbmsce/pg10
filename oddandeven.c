#include <stdio.h>
#include <math.h>
int input()
{
    int a;
    printf("enter a positive integer\n");
    scanf("%d",&a);
    return a;
}
int check(int a)
{
    if (a%2==0)
    return 1;
    else
    return 0;
}
void output(int a, int res)
{
    if (res==1)
    printf("number is even\n");
    else
    printf("number is odd\n");
}
int main()
{
    int a,res;
    a=input();
    res=check(a);
    output(a,res);
    return 0;
}