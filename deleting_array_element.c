#include <stdio.h>
int main()
{
    int n,i,c,k;
    printf("Enter the size of the array\n");
    scanf("%d",&n);
    int a[n];
    printf("Enter the elements of the array\n");
    for(i=0;i<n;i++)
    {
        scanf("%d",&a[i]);
    }
    printf("Enter the index position of the array element to be deleted\n");
    scanf("%d",&k);
    for(i=k;i<n;i++)
    {
        a[i]=a[i+1];
    }
    printf("Array after deleting the required element : ");
    for(i=0;i<n-1;i++)
    {
        printf("%d ",a[i]);
    }
    return 0;
}