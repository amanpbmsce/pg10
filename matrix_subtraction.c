#include <stdio.h>
int main()
{
    int i,j,r,c;
    printf("Enter the number of rows and columns\n");
    scanf("%d%d",&r,&c);
    int a1[r][c],a2[r][c],as[r][c];
    printf("Enter the first matrix\n");
    for(i=0;i<r;i++)
    {
        for(j=0;j<c;j++)
        {
            scanf("%d",&a1[i][j]);
        }
    }
    printf("Enter the second matrix\n");
    for(i=0;i<r;i++)
    {
        for(j=0;j<c;j++)
        {
            scanf("%d",&a2[i][j]);
        }
    }
    
    for(i=0;i<r;i++)
    {
        for(j=0;j<c;j++)
        {
            as[i][j]=a1[i][j]-a2[i][j];
        }
    }
    printf("Subtraction matrix : \n");
    
    for(i=0;i<r;i++)
    {
        for(j=0;j<c;j++)
        {
            printf("%d ",as[i][j]);
        }
        printf("\n");
    }
    return 0;
}