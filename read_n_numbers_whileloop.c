#include <stdio.h>
int main()
{
    int sp=0,sn=0,cp=0,cn=0,cz=0,n;
    float avgp,avgn;
    printf("Enter number ");
    scanf("%d",&n);
    while(n!=-1)
    {
        if(n>0)
        {
            sp=sp+n;
            cp++;
        }
        else if(n<0)
        {
            sn=sn+n;
            cn++;
        }
        else
        {
            cz++;
        }
        printf("Enter number ");
        scanf("%d",&n);
    }
        avgp=(float)sp/cp;
        avgn=(float)sn/cn;
        printf("Sum of positive numbers     : %d\n",sp);
        printf("Average of positive numbers : %.2f\n",avgp);
        printf("Sum of negative numbers     : %d\n",sn);
        printf("Average of negative numbers : %.2f\n",avgn);
        printf("Number of zeros             : %d\n",cz);
        return 0;
}