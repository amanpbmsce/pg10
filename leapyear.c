#include <stdio.h>
#include <math.h>
int input()
{
    int a;
    printf("enter a year\n");
    scanf("%d",&a);
    return a;
}
int check(int a)
{
    if ((a%4==0 && a%100!=0)||(a%400==0))
        return 1;
    else
        return 0;
}
void output(int a, int res)
{
    if (res==1)
        printf("this is a leap year\n");
    else
        printf("this is not a leap year\n");
}
int main()
{
    int a,res;
    a=input();
    res=check(a);
    output(a,res);
    return 0;
}