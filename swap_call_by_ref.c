#include<stdio.h>
int input()
{
    int  a;
    printf("enter the number\t");
    scanf("%d",&a);
    return a;
}
void swap(int *a,int *b)
{
  int t;
  t=*a;
  *a=*b;
  *b=t;
}
int main()
{
  int a,b;
  a=input();
  b=input();
  swap(&a,&b);
  printf("%d %d",a,b);
  return 0;
}
