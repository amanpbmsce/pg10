#include <stdio.h>
#include <math.h>
float input()
{
    float f;
    printf("enter temp. in degree fahrenheit\n");
    scanf("%f",&f);
    return f;
}
float compute(float f)
{
    float c;
    c=(5*(f-32))/9;
    return c;
}
void output(float f,float c)
{
    printf("\ntemp. in degree celcius=%f\n",c);
}
int main()
{
    float f,c;
    f=input();
    c=compute(f);
    output(f,c);
    return 0;
}
